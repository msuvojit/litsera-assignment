import React from "react";
const Home = () => {
  return (
    <div style={{ marginTop: 130 }}>
      <div className="row ">
        <div className="col-md-2"></div>
        <div className="col-md-4">
          <h3
            style={{
              fontSize: "calc(18px + 3vmin)",
              fontWeight: "bold",
              color: "#000000"
            }}
          >
            The very best of Short Stories and Spoken Word Poetry
          </h3>
          <p>
            Discover latest works from top authors and publishers, and create
            you own. Always free, and in your language.
          </p>
          <p>A platform to celebrate the world's ideas.</p>
          <button
            style={{
              backgroundColor: "black",
              color: "white",
              textAlign: "center",
              border: "none",
              width: "100%",
              padding: 8
            }}
            // className="btn btn-dark"
          >
            Get Started
          </button>
        </div>
        <div className="col-md-4">
          <img src="/hero-devices.png" alt="" />
        </div>
      </div>

      <div
        style={{
          marginTop: 60,
          backgroundColor: "#041745",
          color: "white",
          padding: 30,
          marginBottom: 0
        }}
      >
        <div className="row">
          <div className="col-sm-2"></div>

          <div className="col-sm-6">Take us with you</div>
          <div className="col-sm-2">
            <img src="/footer1.png" alt="" />
            {""}
            <img src="/footer2.png" alt="" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
