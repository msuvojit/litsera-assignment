import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from "reactstrap";

const Example = props => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar dark style={{ backgroundColor: "black" }} expand="md">
        <NavbarBrand style={{ marginLeft: "150px" }} href="/">
          PSD WEBSITES
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" style={{ marginRight: "100px" }} navbar>
            <NavItem>
              <NavLink style={{ color: "white" }} href="#">
                Login
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink style={{ color: "white" }} href="#">
                Sign Up
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
};

export default Example;
