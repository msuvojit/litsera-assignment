import React from "react";
import "./App.css";
import Navbar from "./components/Navbar";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Home from "./components/Home";
import SecondPage from "./components/SecondPage";
function App() {
  return (
    <div>
      <Router>
        <Navbar />

        <Route exact path="/" component={Home} />
        <Route exact path="/other" component={SecondPage} />
      </Router>
    </div>
  );
}

export default App;
